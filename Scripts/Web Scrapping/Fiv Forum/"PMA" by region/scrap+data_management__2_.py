#### partie scraping########
import requests
from bs4 import BeautifulSoup
import pandas as pd
import os

base_url = 'https://forum.fiv.fr'
#section_url = f'{base_url}/viewforum.php?f=16'
discussion_urls = [
    #'https://forum.fiv.fr/viewtopic.php?t=1500'
    #'https://forum.fiv.fr/viewtopic.php?t=6008'
    #'https://forum.fiv.fr/viewtopic.php?t=70'
    #'https://forum.fiv.fr/viewtopic.php?t=3997'
    #'https://forum.fiv.fr/viewtopic.php?t=2721'
    #'https://forum.fiv.fr/viewtopic.php?t=11705'
    #'https://forum.fiv.fr/viewtopic.php?t=10520'
    #'https://forum.fiv.fr/viewtopic.php?t=5057'
    #'https://forum.fiv.fr/viewtopic.php?t=2192'
    #'https://forum.fiv.fr/viewtopic.php?t=3846'
    #'https://forum.fiv.fr/viewtopic.php?t=683'
    #'https://forum.fiv.fr/viewtopic.php?t=1930&sid=9dcf3ef13aed0e0a8b4265bd9962858d'
    #'https://forum.fiv.fr/viewtopic.php?t=302&sid=9dcf3ef13aed0e0a8b4265bd9962858d'
    #'https://forum.fiv.fr/viewtopic.php?t=1235&sid=9dcf3ef13aed0e0a8b4265bd9962858d'
    #'https://forum.fiv.fr/viewtopic.php?t=2605'
    #'https://forum.fiv.fr/viewtopic.php?t=2719'
    #'https://forum.fiv.fr/viewtopic.php?t=369'
    #'https://forum.fiv.fr/viewtopic.php?t=8103'
    #'https://forum.fiv.fr/viewtopic.php?t=3080'
    #'https://forum.fiv.fr/viewtopic.php?t=4463'
    #'https://forum.fiv.fr/viewtopic.php?t=4601'
    #'https://forum.fiv.fr/viewtopic.php?t=3263'
    #'https://forum.fiv.fr/viewtopic.php?t=2229'
    #'https://forum.fiv.fr/viewtopic.php?t=903'
    #'https://forum.fiv.fr/viewtopic.php?t=3553'
    #'https://forum.fiv.fr/viewtopic.php?t=2439'
    #'https://forum.fiv.fr/viewtopic.php?t=7579'
    #'https://forum.fiv.fr/viewtopic.php?t=6671'
    'https://forum.fiv.fr/viewtopic.php?t=7166'
    #'https://forum.fiv.fr/viewtopic.php?t=13111'
    #'https://forum.fiv.fr/viewtopic.php?t=6889',
    #'https://forum.fiv.fr/viewtopic.php?t=6889&start=10',
    #'https://forum.fiv.fr/viewforum.php?f=16'
    #'https://forum.fiv.fr/viewtopic.php?t=13541'
    # Ajoutez d'autres URLs de discussion ici si nécessaire
]
response = requests.get(section_url)
soup = BeautifulSoup(response.content, 'html.parser')
# Initialiser une liste pour stocker les données
data = []
# Fonction pour extraire les informations des messages contenant "PMA" ou "FDA"
def extract_messages(soup):
    posts = soup.find_all('div', class_='postbody')
    for post in posts:
        try:
            user = post.find('p', class_='author').find('a', class_='username')
            if user:
                user = user.text.strip()
            else:
                user = 'N/A'
            
            date = post.find('p', class_='author').find('time')
            if date:
                date = date['datetime']
            else:
                date = 'N/A'

            content = post.find('div', class_='content')
            if content:
                content = content.get_text(strip=True)
            else:
                content = 'N/A'
            
            # Rechercher des mentions de "PMA" ou "FDA"
            if 'PMA' in content or 'FDA' in content:
                data.append({
                    'Utilisateur': user,
                    'Date': date,
                    'Contenu': content
                })
        except Exception as e:
            print(f"Error extracting post details: {e}")
            continue
# Fonction pour traiter une discussion et extraire les messages de toutes les pages
def process_discussion(discussion_url):
    print(f'Processing discussion: {discussion_url}')
    while discussion_url:
        discussion_response = requests.get(discussion_url)
        discussion_soup = BeautifulSoup(discussion_response.content, 'html.parser')
        extract_messages(discussion_soup)

        # Vérifier s'il y a une page suivante
        pagination = discussion_soup.find('div', class_='pagination')
        next_page = None
        if pagination:
            next_link = pagination.find('a', rel='next')
            if next_link:
                next_page = next_link['href']
        
        if next_page:
            discussion_url = f'{base_url}/{next_page}'
        else:
            discussion_url = None

# Parcourir chaque discussion dans la liste et extraire les messages
for discussion_url in discussion_urls:
    process_discussion(discussion_url)

# Convertir les données en DataFrame
df = pd.DataFrame(data)

# Définir le chemin complet pour enregistrer le fichier CSV
csv_filename = 'C:/Users/pc/Desktop/zitouni/Sud Provence-Alpes-Côte d’Azur/Sud Provence-Alpes-Côte d’Azur_pma3_fda_data.csv'

# Sauvegarder les données dans un fichier CSV
df.to_csv(csv_filename, index=False)

print("Scraping terminé et données sauvegardées dans", csv_filename)

######## partie data management #############################

import pandas as pd

# Dictionnaire des chemins de fichiers et des noms de région correspondants
fichiers_regions = {
    "Auvergne - Rhône-Alpes": [
        "C:/Users/pc/Desktop/projet_zitouni/Auvergne - Rhône-Alpes.csv"
    ],
    "Bourgogne_Franche_Comté": [
        "C:/Users/pc/Desktop/projet_zitouni/Bourgogne_Franche_Comté.csv"
    ],
    "Grand Est": [
        "C:/Users/pc/Desktop/projet_zitouni/Grand_est.csv",
    ],
    "Hauts de France": [
        "C:/Users/pc/Desktop/projet_zitouni/Hauts_de_France.csv"
    ],
    "Île de France": [
        "C:/Users/pc/Desktop/projet_zitouni/Ile_de_France.csv"
    ],
    "Normandie": [
        "C:/Users/pc/Desktop/projet_zitouni/Normandie.csv"
    ],
    "Nouvelle-Aquitaine": [
        "C:/Users/pc/Desktop/projet_zitouni/Nouvelle_aquitaine.csv"
    ],
    "Occitanie": [
        "C:/Users/pc/Desktop/projet_zitouni/Occitanie.csv"
    ],
    "Pays de la Loire": [
        "C:/Users/pc/Desktop/projet_zitouni/Pays_de_la_Loire.csv"
    ],
    "Sud Provence-Alpes-Côte d'Azur": [
        "C:/Users/pc/Desktop/projet_zitouni/Sud_Provence_Alpes_Cote_d_Azur.csv"
    ]
}

# Liste pour stocker les DataFrames
dataframes = []

# Parcourir chaque région et ses fichiers
for region, fichiers in fichiers_regions.items():
    # Lire et concaténer les fichiers CSV pour chaque région
    region_df = pd.concat([pd.read_csv(fichier) for fichier in fichiers])
    # Ajouter une colonne 'Region' avec le nom de la région
    region_df['Region'] = region
    # Ajouter le DataFrame à la liste
    dataframes.append(region_df)

# Concatenation de tous les DataFrames régionaux
final_df = pd.concat(dataframes)
########Nettoyage###############"""
import unicodedata
import re
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer

# Utiliser les stopwords en français
stop_words = set(stopwords.words('french'))

lemmatizer = WordNetLemmatizer()

# Fonction pour enlever les accents
def enlever_accents(texte):
    return ''.join(c for c in unicodedata.normalize('NFD', texte) if unicodedata.category(c) != 'Mn')

# Fonction de nettoyage du texte
def nettoyer_texte(texte):
    if isinstance(texte, str):
        # Convertir le texte en minuscules
        texte = texte.lower()
        
        # Supprimer les accents
        texte = enlever_accents(texte)
        
        # Supprimer les lettres isolées et les caractères non alphanumériques
        texte = re.sub(r'\b[a-zA-Z]\b', '', texte)
        texte = re.sub(r'[^\w\s]', '', texte)
        
        # Supprimer les espaces en trop
        texte = ' '.join(texte.split())
        
        # Tokenizer le texte
        tokens = word_tokenize(texte)
        
        # Garder seulement les mots alphabétiques
        tokens = [word for word in tokens if word.isalpha()]
        
        # Supprimer les stopwords
        tokens = [word for word in tokens if word not in stop_words]
        
        # Lemmatiser les mots (utilisation de lemmatizer pour l'anglais par manque de support natif en français)
        tokens = [lemmatizer.lemmatize(word) for word in tokens]
        
        # Reconstituer le texte nettoyé
        texte = ' '.join(tokens)
        
    return texte

# Supprimer les colonnes "Date" et "Utilisateur"
final_df.drop(columns=["Date", "Utilisateur"], inplace=True)

# Appliquer le nettoyage du texte à la colonne du dataset
final_df['Contenu'] = final_df['Contenu'].apply(nettoyer_texte)
# Supprimer les doublons
final_df = final_df.drop_duplicates(subset='Contenu')

import nltk
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans
# Vectorisation des textes
vectorizer = TfidfVectorizer()
X = vectorizer.fit_transform(final_df['Contenu'])

# Clustering des textes
kmeans = KMeans(n_clusters=2, random_state=42)  # Ajustez le nombre de clusters si nécessaire
final_df['cluster'] = kmeans.fit_predict(X)

# Afficher quelques exemples de chaque cluster pour analyse manuelle
for i in range(2):  # Ajustez selon votre nombre de clusters
    print(f"\nCluster {i}:")
    print(final_df[final_df['cluster'] == i]['Contenu'].head(10))

# le cluster 1 représente les réalisations
realisation_cluster = 1
realisation_data =final_df[final_df['cluster'] == realisation_cluster]

# Afficher les résultats filtrés
print(realisation_data)

# Dictionnaire de termes indiquant la réalisation effective de la PMA ou FIV
realisation_terms = {
    'PMA': [
        'jai fait une pma', 'nous avons fait une pma', 'jai realise une pma', 'nous avons realise une pma',
        'pma reussie', 'pma effectuee', 'premiere pma', 'commence pma', 
        'pma demarree', 'debut pma', 'pma commencee', 'pma en cours', 'traitement pma', 'cycle pma', 'seance pma',
        'demarre notre parcours pma', 'parcours pma commence', 'pma realisee avec succes', 'suivi','je fais pma'
    ],
    'FIV': [
        'jai fait une fiv', 'nous avons fait une fiv', 'jai realise une fiv', 'nous avons realise une fiv',
        'fiv reussie', 'fiv effectuee', 'premiere fiv', 'commence fiv',
        'fiv demarree', 'debut fiv', 'fiv commencee', 'fiv en cours', 'traitement fiv', 'cycle fiv', 'seance fiv',
        'jai fait ma premiere fiv', 'fiv realisee avec succes', 'parcours fiv demarre','je fais fiv', 'suivi fiv'
    ]
}

# Afficher le dictionnaire mis à jour
print(realisation_terms)

realisation_data = final_df[(final_df['PMA_realised'] == True) | (final_df['FIV_realised'] == True)]

# Afficher les résultats filtrés
print(realisation_data)
# Compter les réalisations par région
occurrences = realisation_data.groupby('Region').apply(
    lambda x: pd.Series({
        'PMA_count': x['PMA_realised'].sum(),
        'FIV_count': x['FIV_realised'].sum()
    })
).reset_index()

# Affichage des résultats pour vérifier
print(occurrences)
# Enregistrer le DataFrame en fichier CSV
occurrences.to_csv(C:/Users/pc/Desktop/projet_zitouni/data.csv, index=False)

print(f"Fichier CSV enregistré avec succès à {chemin}.")

#####importation des donnees regions #####
import geopandas as gpd

# Chemin vers votre fichier GeoJSON
geojson_path = 'C:/Users/pc/Desktop/projet_zitouni/regions-version-simplifiee.geojson'  # Remplacez par le chemin correct

# Lire le fichier GeoJSON
gdf = gpd.read_file(geojson_path)

# Afficher les premières lignes pour vérifier le résultat
print(gdf.head())

# Afficher le GeoDataFrame complet
print(gdf)

# Afficher le GeoDataFrame de manière interactive (si vous utilisez Jupyter Notebook)
from IPython.display import display
display(gdf)

# Renommer les colonnes pour qu'elles correspondent
gdf = gdf.rename(columns={'nom': 'Region'})
occurrences = occurrences.rename(columns={'Region': 'Region'})

# Corriger les noms des régions pour correspondre entre les deux datasets si nécessaire
occurrences['Region'] = occurrences['Region'].replace({
    'Auvergne - Rhône-Alpes': 'Auvergne-Rhône-Alpes',
    'Bourgogne_Franche_Comté': 'Bourgogne-Franche-Comté',
    'Hauts de France': 'Hauts-de-France',
    'Sud Provence-Alpes-Côte d\'Azur': 'Provence-Alpes-Côte d\'Azur',
    'Île de France': 'Île-de-France',
    'Centre-Val de Loire': 'Pays de la Loire'
})
# Enregistrer le DataFrame en fichier CSV
occurrences.to_csv('C:/Users/pc/Desktop/projet_zitouni/data.csv', index=False)

print(f"Fichier CSV enregistré avec succès")

# Fusionner les deux DataFrames
merged_gdf = gdf.merge(occurrences, on='Region', how='left')

# Afficher les premières lignes pour vérifier la fusion
print(merged_gdf.head())