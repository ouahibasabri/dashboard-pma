import requests
from bs4 import BeautifulSoup
import pandas as pd
import os

base_url = 'https://forum.fiv.fr'
section_url = f'{base_url}/viewforum.php?f=5'

# Fonction pour obtenir toutes les URLs de discussions dans une section
def get_discussion_urls(section_url):
    discussion_urls = []
    while section_url:
        response = requests.get(section_url)
        soup = BeautifulSoup(response.content, 'html.parser')
        
        # Extraire les URLs des discussions
        discussions = soup.find_all('a', class_='topictitle')
        for discussion in discussions:
            discussion_url = f"{base_url}/{discussion['href']}"
            discussion_urls.append(discussion_url)
        
        # Vérifier s'il y a une page suivante dans la section
        pagination = soup.find('div', class_='pagination')
        next_page = None
        if pagination:
            next_link = pagination.find('a', rel='next')
            if next_link:
                next_page = next_link['href']
        
        if next_page:
            section_url = f'{base_url}/{next_page}'
        else:
            section_url = None
    
    return discussion_urls

# Fonction pour extraire les informations des messages contenant "PMA" ou "FIV"
def extract_messages(soup):
    posts = soup.find_all('div', class_='postbody')
    for post in posts:
        try:
            user = post.find('p', class_='author').find('a', class_='username')
            if user:
                user = user.text.strip()
            else:
                user = 'N/A'
            
            date = post.find('p', class_='author').find('time')
            if date:
                date = date['datetime']
                # Arrêter si la date est de 2014 ou avant
                if int(date[:4]) < 2014:
                    return False
            else:
                date = 'N/A'

            content = post.find('div', class_='content')
            if content:
                content = content.get_text(strip=True)
            else:
                content = 'N/A'
            
            # Rechercher le titre du commentaire
            title = post.find('h3', class_='first')
            if title:
                title = title.get_text(strip=True)
            else:
                title = 'N/A'
            
            # Rechercher des mentions de "PMA" ou "FIV"
            if 'PMA' in content or 'FIV' in content:
                data.append({
                    'Titre': title,
                    'Utilisateur': user,
                    'Date': date,
                    'Contenu': content
                })
        except Exception as e:
            print(f"Error extracting post details: {e}")
            continue
    return True

# Fonction pour traiter une discussion et extraire les messages de toutes les pages
def process_discussion(discussion_url):
    print(f'Processing discussion: {discussion_url}')
    while discussion_url:
        discussion_response = requests.get(discussion_url)
        discussion_soup = BeautifulSoup(discussion_response.content, 'html.parser')
        if not extract_messages(discussion_soup):
            break

        # Vérifier s'il y a une page suivante
        pagination = discussion_soup.find('div', class_='pagination')
        next_page = None
        if pagination:
            next_link = pagination.find('a', rel='next')
            if next_link:
                next_page = next_link['href']
        
        if next_page:
            discussion_url = f'{base_url}/{next_page}'
        else:
            discussion_url = None

# Initialiser une liste pour stocker les données
data = []

# Obtenir toutes les URLs des discussions dans la section
discussion_urls = get_discussion_urls(section_url)

# Parcourir chaque discussion dans la liste et extraire les messages
for discussion_url in discussion_urls:
    process_discussion(discussion_url)

# Convertir les données en DataFrame
df = pd.DataFrame(data)

# Définir le chemin complet pour enregistrer le fichier CSV
csv_filename = './enceinte_pma.csv'

# Sauvegarder les données dans un fichier CSV
df.to_csv(csv_filename, index=False)

print("Scraping terminé et données sauvegardées dans", csv_filename)
